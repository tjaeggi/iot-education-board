# INHALT
Hier befinden sich Dokumente zum IoT-Education-Board: 

- Manual zum Board
- Arduino Demo-Sketches, um alle Komponenten in Betrieb zu nehmen
- Datasheets der Komponenten auf dem Board
- Software-Komponenten; sind im Manual erwähnt.

# Literatur zum Thema ESP32-Programmierung mit Arduino-IDE
gute Quelle: http://randomnerdtutorials.com

## E-Books:
- Learn ESP32 with Arduino IDE 2nd Edition - Rui Santos
- ESP32 WEB SERVER WITH ARDUINO IDE Step-by-Step Project Guide - Rui Santos 

## Buch und E-Book:
- Mikrocontroller ESP32 Das umfassende Handbuch - Udo Brandes - Rheinwerk Verlag
- Entdecke den IoT-Chip-Das offizielle ESP32-Handbuch - Dogan Ibrahim - Elektor Verlag
- The Complete ESP32 Projects Guide - Elektor Verlag


Thomas Jäggi - gibb Berufsfachschule Bern

thomas.jaeggi(at)m121.ch

