/*
 * Function:  
 * Reads Sensor-Data from BME280 and sends them with NRF24L01 over 2.4GHz to the Gateway. 
 * 
 * 
 * Use Sketch together with NRF24L01ESP32GatewayThingspeak.ino
 * 
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      NRF24L01SensorNodeSender.ino
 * Libraries to install:   RF24 TMRh20 2020 - Optimized fork of the nRF24L01+ driver
 *              Adafruit_Sensor and Adafruit_BME280
 * Connections:
 * 
 * NRF24L01     ESP32
 *===================
 *CE            4
 *SCK           18
 *MISO          19
 *CNS=CSN       5
 *MOSI          23
 *IRQ           not used
 *              
 * Power activate: Jumper over Marker NRF24L01 3V3
 * 
 * 
 * Output:    Serial Monitor with 115200baud and thingspeak.com
 * Author:    Thomas Jäggi
 * Date:      05.01.2022
 * 
 */
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <nRF24L01.h>
#include <RF24.h>
 
RF24 radio(4, 5); // CE, CSN on ESP32
const uint64_t address = 0xF0F0F0F0E1LL;  // Make sure, Receiver has the same address!!
// Adresse ist hier als HEX-Wert dargestellt. Kann beliebig geändert werden.
// Der Empfänger muss dieselbe Adresse haben.
// Bedeutung von LL am Ende: LL is the suffix for long-long, 
// which is 64-bit on most C/C++ implementations. 
// So 0LL is a 64-bit literal with the value of 0.

int counter = 0;
 
 
float temperature;
float humidity;
float altitude;
float pressure;
 
#define SEALEVELPRESSURE_HPA (1015.20)
// see Sealevel-Pressure of today: https://meteologix.com/observations/pressure-qnh.html or better here:
// https://www.meteoswiss.admin.ch/home/measurement-values.html?param=messwerte-luftdruck-qnh-10min&station=PAY&chart=hour
Adafruit_BME280 bme;
 
struct MyData 
{
  int counter;
  float temperature;
  float humidity;
  float altitude;
  float pressure;
};
MyData data;
 
void setup() 
{
Serial.begin(115200);
radio.begin();                  //Starting the Wireless communication
radio.openWritingPipe(address); //Setting the address where we will send the data
radio.setPALevel(RF24_PA_MIN);  //You can set it as minimum or maximum depending on the distance between the transmitter and receiver.
radio.stopListening();          //This sets the module as transmitter
 
if (!bme.begin(0x76)) 
{
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1);
  }
 
}
 
void loop()
{
  data.counter = counter;
  data.temperature = bme.readTemperature();
  data.pressure = bme.readPressure() / 100.0F;
  data.altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
  data.humidity = bme.readHumidity();
  
  Serial.print("Packet No. = ");
  Serial.println(data.counter);
  
  Serial.print("Temperature = ");
  Serial.print(data.temperature);
  Serial.println("*C");
 
  Serial.print("Pressure = ");
  Serial.print(data.pressure);
  Serial.println("hPa");
 
  Serial.print("Approx. Altitude = ");
  Serial.print(data.altitude);
  Serial.println("m");
 
  Serial.print("Humidity = ");
  Serial.print(data.humidity);
  Serial.println("%");
 
  Serial.println();
  
  radio.write(&data, sizeof(MyData));
  
  Serial.println("Data Packet Sent");
  Serial.println("");
  
  counter++;
  delay(5000);
}
