/*
 Title:       Detect Motion with HC-SR501
 
 Sketchfile:  HC-SR501-LED.ino
 
 Libraries used: none
 
 Function:    This tutorial shows how to detect motion with the ESP32 using a PIR motion sensor. 
              In this example, when motion is detected (an interrupt is triggered), the ESP32 starts 
              a timer and turns an LED on for a predefined number of seconds. When the timer finishes 
              counting down, the LED is automatically turned off.
 
 Connections: Activate Power on HC-SR501 5V PIR
                                
              Jumper Cable Connections:
                                HC-SR501_Out mit GPIO27
                                LED red mit      GPIO26

siehe auch: https://randomnerdtutorials.com/esp32-pir-motion-sensor-interrupts-timers/
 
 Date:        tja/05.12.2021
*/

#define timeSeconds 10

// Set GPIOs for LED and PIR Motion Sensor
const int led = 26;
const int motionSensor = 27;

// Timer: Auxiliary variables
unsigned long now = millis();
unsigned long lastTrigger = 0;
boolean startTimer = false;

// Checks if motion was detected, sets LED HIGH and starts a timer
void IRAM_ATTR detectsMovement() {
  Serial.println("MOTION DETECTED!!!");
  digitalWrite(led, HIGH);
  startTimer = true;
  lastTrigger = millis();
}

void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);
  
  // PIR Motion Sensor mode INPUT_PULLUP
  pinMode(motionSensor, INPUT_PULLUP);
  // Set motionSensor pin as interrupt, assign interrupt function and set RISING mode
  attachInterrupt(digitalPinToInterrupt(motionSensor), detectsMovement, RISING);

  // Set LED to LOW
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);
}

void loop() {
  // Current time
  now = millis();
  // Turn off the LED after the number of seconds defined in the timeSeconds variable
  if(startTimer && (now - lastTrigger > (timeSeconds*1000))) {
    Serial.println("Motion stopped...");
    digitalWrite(led, LOW);
    startTimer = false;
  }
}
