// Function: simple Tone for passive Buzzer using PWM
//
// Connections: BuzzerIO with GPIO21
// Activate Power for Buzzer: Jumper over white Marker 
// Buzzer Module VCC 3V3
// Date: 21.12.2021
// Author: Thomas Jäggi
/////////////////////////////////////////////////////
#define buzzer_pin 21
int freq = 4000;
int ledChannel = 0;
int resolution = 10;

void setup() {
  ledcSetup(ledChannel, freq, resolution);  //Sets the frequency and number of counts corresponding to the channel.
  ledcAttachPin(buzzer_pin, ledChannel); //Binds the specified channel to the specified I/O port for output.
  ledcWrite(ledChannel, 512);  //Output PWM.
}

void loop() {
    ledcWrite(ledChannel, 512);  //Output PWM.
    delay(3000);
    ledcWrite(ledChannel, 0);  //Output PWM.
    delay(3000);
}
