  /*
 Title:       read analog Signal from Potentiometer
 Sketchfile:  ReadPotentiometer.ino
 
 Function:    Read analog Signal from Poti and convert it into Voltage, using ADC.
 Connections: Potentiometer is connected to GPIO 34 (Analog ADC1_CH6)

 Date:        tja/15.10.2021

*/


const int potPin = 34;

// variable for storing the potentiometer value
int potValue = 0;

void setup() {
  Serial.begin(115200);
  delay(1000);
  analogSetPinAttenuation(potPin, ADC_11db);
  analogReadResolution(12);
  analogSetWidth(12);
  
}

void loop() {
  // Reading potentiometer value
  potValue = analogRead(potPin);
  float voltage = potValue * (3.3 / 4095.0);
  Serial.print("ADC-Value: ");Serial.print(potValue); Serial.print(" relates to ");Serial.print(voltage);Serial.println(" Volt");
  
  delay(500);
}
