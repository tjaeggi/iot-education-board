/*
 Title:       read analog Signal from TMP36 with ADS1115 - Analog Digital Converter
 
 Sketchfile:  TMP36ADC1115SingleEnded_with_GAIN4x.ino
 
 Libraries used: Wire for I2C, Adafruit_ADS1X15 for ADS1115
 
 Function:    Read analog Signal from TMP36 and convert it into Voltage, using ADS1115.
              Temperature is derived from Voltage with Formula: temp = (voltage - 0.5) * 100 ; 
 
 Connections: TMP36 Vout is connected to A0 of ADS1115
              SDA ADS1115 to SDA of ESP32
              SCL ADS1115 to SCL of ESP32
 

 Date:        tja/04.12.2021

*/



#include <Wire.h>
#include <Adafruit_ADS1X15.h>

Adafruit_ADS1115 ads1115;

  float volts0=0.000;
  float volts1=0.000;
  float temp=0.000;


void setup(void)
{
  Serial.begin(115200);
  Serial.println("Hello!");
  
  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV)");
  // Set GAIN:
  // TMP36 Input is about 1V. With GAIN_FOUR we push this range over 16Bits and get much precise measurements than with
  // 16Bits over +/- 6.144 Volt (=default)
  ads1115.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.03125mV
                                    // push the 1V input signal over the entire 16-bit range of the ADC
  // other GAINS - For TMP36: GAIN_FOUR is perfect!
  // ads1115.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 0.1875mV (default, no gain specified)
  // ads1115.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 0.125mV
  // ads1115.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 0.0625mV
  // ads1115.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.03125mV
  // ads1115.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.015625mV
  // ads1115.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.0078125mV
  
  if (!ads1115.begin()) {
    Serial.println("Failed to initialize ADS1115.");
    while (1);
  }
}

void loop(void)
{
  int16_t adc0;

  adc0 = ads1115.readADC_SingleEnded(0);
  //adc1 = ads1115.readADC_SingleEnded(1);
  //adc2 = ads1115.readADC_SingleEnded(2);
  //adc3 = ads1115.readADC_SingleEnded(3);
  volts0 = (ads1115.computeVolts(adc0))*1.00888888888888888; // Korrekturwert für Raumtemperatur. experimentell bestimmt. 
                                                             // Mit diesem Korrekturwert erhalten wir absolut
                                                             // präzise Messungen. ~Millivoltgenauigkeit!
                                                             // Muss für jeden einzelnen ADC individuell bestimmt werden.
  //volts1 = ads1115.computeVolts(adc1);
  //volts2 = ads1115.computeVolts(adc2);
  //volts3 = ads1115.computeVolts(adc3);

  temp = (volts0 - 0.5) * 100 ;  
  
  //Serial.print("AIN0: "); Serial.print(adc0);Serial.print("  ");Serial.println(volts0,3);
  //Serial.print("AIN1: "); Serial.print(adc1);Serial.print(" relates to ");
  Serial.print(volts0,3);Serial.print(" -->");Serial.print(temp,3);Serial.println("*C");
  //Serial.print("AIN2: "); Serial.println(adc2);
  //Serial.print("AIN3: "); Serial.println(adc3);
  Serial.println(" ");
  
  delay(1000);
}
