/*
===============================================================================================================
SimpleRotary.h Library Options Example Sketch
Learn more at [https://github.com/mprograms/SimpleRotary]
This example shows how to set various options.
=========================================================================
PREPARATION
=========================================================================
File: RotEncoderCW-CCW-CounterSerMon.ino
Pixelring needs 5V from DC-DC Step-Down Module

activate Power: Place red Jumper over white Field on:
            
            RotaryEncoder 3V3

Connections with KY-040: 
             DT  to GPIO32
             CLK to GPIO21
             SW  to GPIO25
            
Connections with EC11:
             A  to GPIO32
             B to GPIO21
             C  to GPIO25
            


Function: By turning the rotaryEncoder you can see the counter in Serial Monitor
          


*/
#include <SimpleRotary.h>

// Pin A, Pin B, Button Pin
SimpleRotary rotary(32,21,25);

int z = 0;


void setup() {

  // Set the trigger to be either a HIGH or LOW pin (Default: HIGH)
  // Note this sets all three pins to use the same state.
  rotary.setTrigger(HIGH);

  // Set the debounce delay in ms  (Default: 2)
  rotary.setDebounceDelay(5);

  // Set the error correction delay in ms  (Default: 200)
  rotary.setErrorDelay(250);
  Serial.begin(115200);
}

void loop() {
  byte i,k;
  i = rotary.rotate();
  k = rotary.push();
  // Only print CW / CCW output to prevent an endless stream of output.
  if ( i == 1 ) {
    Serial.println(z++);
  }
  if ( i == 2) {
    Serial.println(z--);
  }
  if ( k == 1 ) {
    Serial.println("Pushed - Counter Reset");
    z = 0;
    Serial.println(z);
  }
}
