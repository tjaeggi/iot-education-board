/*
 Title:       read analog Signal from Potentiometer and display ADC-Value and Voltage onto OLED Display
 Sketchfile:  ReadPotiOLEDout.ino
 
 Function:    Read analog Signal from Poti and convert it into Voltage, using ADC.
 Connections: Potentiometer is connected to GPIO 34 (Analog ADC1_CH6)
              OLED SDA-Pin to SDA Pin Microcontroller GPIO21
              OLED SCL-Pin to SCL Pin Microcontroller GPIO22
              Jumper "OLED 3V3" is active: Place Jumper over white field.

 Date:        tja/15.10.2021

*/

/* OLED Settings
 *  
 */

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);




const int potPin = 34;

// variable for storing the potentiometer value
int potValue = 0;

void setup() {
  Serial.begin(115200);
    // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }
  // Clear the buffer
  display.clearDisplay();
  delay(1000);
  analogSetPinAttenuation(potPin, ADC_11db);
  analogReadResolution(12);
  analogSetWidth(12);
  
}

void loop() {
  // Reading potentiometer value
  potValue = analogRead(potPin);
  float voltage = potValue * (3.3 / 4095.0);
  display.clearDisplay();
  display.setTextSize(2);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.cp437(true);         // Use full 256 char 'Code Page 437' font
  display.print(potValue); display.println(" ADC");display.print(voltage);display.println(" Volt");
  display.display();
  delay(500);
}
