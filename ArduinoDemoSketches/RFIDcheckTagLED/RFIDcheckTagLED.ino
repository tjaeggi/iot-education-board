/*
 * Function:  DUMP RFID Card-Info and compare UID: ok: LED green; not ok: LED red
 * MCU:       Olimex ESP32-DevKit-LiPo
 * Connections: 
 *             RFID Module        ESP32
 *             ========================
 *             SDA                CS-5
 *             SCK                CLK-18
 *             MOSI               MOSI-23
 *             MISO               MISO-19
 *             RST                GPIO22
 *             =========================
 *             LED connections
 *             =========================
 *             LED red            GPIO4
 *             LED green          GPIO2
 *             
 * Power On:  Jumper over white Marker Pins "RFID 3V3"
 * File:      RFIDcheckTagLED.ino
 * Based on the RFID EXample.
 * Author:    Thomas Jäggi
 * Date:      13.12.2021
 */

#include <SPI.h>
#include <MFRC522.h>                // für RFID-RC522

#define LEDr_PIN        4           // LED-Pin
#define LEDgn_PIN       2           // LED-Pin
#define RST_PIN        22          // RST-Pin
#define SDA_PIN         5          // SS- bzw. SDA-Pin

MFRC522 mfrc522(SDA_PIN, RST_PIN);  // MFRC522-Instanz erstellen

void setup() {
  Serial.begin(115200);   
  SPI.begin();                      // SPI-Bus initialisieren
  mfrc522.PCD_Init();               // RFID-RC522 initialisieren
  Serial.println("Ein RFID-Tag an den Leser halten ...");
  Serial.println();
  pinMode(LEDr_PIN, OUTPUT);
  pinMode(LEDgn_PIN, OUTPUT);
  digitalWrite(LEDr_PIN, LOW);
  digitalWrite(LEDgn_PIN, LOW);
}
void loop() {
  String rfidTag= "";
  if (mfrc522.PICC_IsNewCardPresent() &&        // liegt ein neues Tag vor
    mfrc522.PICC_ReadCardSerial() ) {           // richtige Tag-Art
    Serial.print("RFID-Tag/Karten-ID:");
    for (byte i = 0; i < mfrc522.uid.size; i++)  {   //  Karten-ID extrah.
      Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
      Serial.print(mfrc522.uid.uidByte[i], HEX);
      rfidTag.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));  
      rfidTag.concat(String(mfrc522.uid.uidByte[i], HEX));
    }
    Serial.println();
    Serial.print("Aktion: ");
    rfidTag.toUpperCase();
    if (rfidTag.substring(1) == "41 8E B9 45") {    // zuzulassendes RFID-Tag 
      Serial.println("Zugang genehmigt \n");
      digitalWrite(LEDr_PIN, LOW);
      digitalWrite(LEDgn_PIN, HIGH);
      delay(3000);
    } else {
      Serial.println("Zugang verweigert!\n");
      digitalWrite(LEDgn_PIN, LOW);
      digitalWrite(LEDr_PIN, HIGH);
      delay(3000);
    }
  }
}
