/*
===============================================================================================================
SimpleRotary.h Library Options Example Sketch
Learn more at [https://github.com/mprograms/SimpleRotary]
This example shows how to set various options.
=========================================================================
PREPARATION
=========================================================================
Plug in DC Power Adapter - we need 5V from DC-DC-Step-Down-Module

activate Power: Place red Jumper over white Field on:
            PixelRing 5Vext
            RotaryEncoder 3V3

Connections with KY-040: 
             DT  to GPIO32
             CLK to GPIO21
             SW  to GPIO25
             NeoPixel Din to GPIO5

Connections with EC11:
             A  to GPIO32
             B to GPIO21
             C  to GPIO25
             NeoPixel Din to GPIO5


Function: By turning the rotaryEncoder the Pixels light up each after the other as long as you turn. Direction is considered.
          


*/

#include <SimpleRotary.h>
// Pin A (DT), Pin B (CLK), Button Pin(SW)
SimpleRotary rotary(32,21,25);


#include <Adafruit_NeoPixel.h>
#define PIXEL_PIN    5  // Digital IO pin connected to the NeoPixels.
#define PIXEL_COUNT 12  // Number of NeoPixels 12 or 16
// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)


int z = 0;


void setup() {
   strip.begin(); // Initialize NeoPixel strip object (REQUIRED)
   strip.show();  // Initialize all pixels to 'off'
  
  // Set the trigger to be either a HIGH or LOW pin (Default: HIGH)
  // Note this sets all three pins to use the same state.
  rotary.setTrigger(HIGH);

  // Set the debounce delay in ms  (Default: 2)
  rotary.setDebounceDelay(5);

  // Set the error correction delay in ms  (Default: 200)
  rotary.setErrorDelay(250);
  Serial.begin(115200);
}

void loop() {
  byte i,k;
  i = rotary.rotate();
  k = rotary.push();
  // Only print CW / CCW output to prevent an endless stream of output.
  if ( i == 1 ) {
    Serial.println(z++);
    if ( z > (PIXEL_COUNT-1) ) {z=0;}
    strip.setPixelColor(z, strip.Color(255,   0,   0)); strip.show();delay(50); 
    strip.setPixelColor(z-1, strip.Color(0,   0,   0)); strip.show();delay(50); 
    if ( z == 0 ) {strip.setPixelColor((PIXEL_COUNT-1), strip.Color(0,   0,   0)); strip.show();delay(50); }
    if ( z > (PIXEL_COUNT-1) ) {strip.show();}  // Initialize all pixels to 'off'}
  }
  
  if ( i == 2) {
    Serial.println(z--);
    if ( z < 0 ) {z=PIXEL_COUNT-1;}
    strip.setPixelColor(z, strip.Color(255,   0,   0)); strip.show();delay(50); 
    strip.setPixelColor(z+1, strip.Color(0,   0,   0)); strip.show();delay(50); 
    if ( z == PIXEL_COUNT-1 ) {strip.setPixelColor((0), strip.Color(0,   0,   0)); strip.show();delay(50); }
    
  }

  
  if ( k == 1 ) {

    for(int j=0; j<strip.numPixels(); j++) { // For each pixel in strip...
    strip.setPixelColor(j, strip.Color(0,   0,   0));         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(50);                           //  Pause for a moment
    }

    
    Serial.println("Pushed - Counter Reset");
    z = 0;
    Serial.println(z);
    for(int j=0; j<strip.numPixels(); j++) { // For each pixel in strip...
    strip.setPixelColor(j, strip.Color(0,   255,   0));         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(50);                           //  Pause for a moment
  }
  }
}
