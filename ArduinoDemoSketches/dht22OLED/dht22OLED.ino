/*
 * Function:  Auslesen der Feuchtigkeit und Temperatur mit dem Sensor DHT22
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      dht22OLED.ini
 * Library:   DHT sensor, Adafruit GFX und Adafruit SSD106 Library wird verwendet.
 * Connections: "DHT22 Dout" mit GPIO 16 verbinden. SCL und SDA from OLED to Microcontroller.
 *              Activate Power with Jumper "OLED 3V3" and "DHT22 3V3". Place Jumper over white field.
 * Output:    Serial Monitor with 115200baud and OLED-Display
 * Author:    Thomas Jäggi
 * Date:      30.07.2021
 */

/************************( Importieren der genutzten Bibliotheken )************************/
#include "DHT.h"                
#define DHTPIN 16         // Hier die Pin Nummer eintragen wo der Sensor angeschlossen ist
#define DHTTYPE DHT22    // Hier wird definiert was für ein Sensor ausgelesen wird. In 
                          // unserem Beispiel möchten wir einen DHT22 auslesen, falls du 
                          // ein DHT22 hast einfach DHT11 eintragen
//*************** OLED Settings ******************
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     4 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32

//********************************( Definieren der Objekte )********************************/                          
DHT dht(DHTPIN, DHTTYPE);
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void setup() {
  Serial.begin(115200);
  Serial.println("DHT22 Testprogramm");
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
     Serial.println(F("SSD1306 allocation failed"));
     for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay();
  dht.begin();
}
void loop() {
  // Wait a few seconds between measurements.
  delay(2000);                     // Hier definieren wir die Verweilzeit die gewartet wird  
                                   // bis der Sensor wieder ausgelesen wird. Da der DHT22 
                                   // auch ca. 2 Sekunden hat um seine Werte zu aktualisieren
                                   // macht es keinen Sinn ihn schneller auszulesen!
                                    
  float h = dht.readHumidity();    // Lesen der Luftfeuchtigkeit und speichern in die Variable h
  float t = dht.readTemperature(); // Lesen der Temperatur in °C und speichern in die Variable t
  
/*********************( Überprüfen ob alles richtig Ausgelesen wurde )*********************/ 
  if (isnan(h) || isnan(t)) {       
    Serial.println("Fehler beim Auslesen des Sensors!");
    display.clearDisplay();
    display.setTextSize(2);      // Normal 1:1 pixel scale
    display.setTextColor(SSD1306_WHITE); // Draw white text
    display.setCursor(0, 0);     // Start at top-left corner
    display.cp437(true);         // Use full 256 char 'Code Page 437' font
    display.print("Fehler beim Auslesen des Sensors!");
    display.display();
    return;
  }

  // Nun senden wir die gemessenen Werte an den PC dise werden wir im Seriellem Monitor sehen
    display.clearDisplay();
    display.setTextSize(1);      // Normal 1:1 pixel scale
    display.setTextColor(SSD1306_WHITE); // Draw white text
    display.setCursor(0, 0);     // Start at top-left corner
    //display.cp437(true);         // Use full 256 char 'Code Page 437' font    
  Serial.print("Luftfeuchtigkeit: ");
  Serial.print(h);                  // Ausgeben der Luftfeuchtigkeit
  Serial.print("%\t");              // Tabulator
  Serial.print("Temperatur: ");
  Serial.print(t);                  // Ausgeben der Temperatur
  Serial.print("*");                // Schreiben des ° Zeichen
  Serial.println("C");
// OLED Ausgabe:
  display.println("Humidity:");
  display.setTextSize(2);
  display.print(h);  display.println("%"); // Ausgeben der Luftfeuchtigkeit
  display.setTextSize(1);
  display.println("Temperatur:");
  display.setTextSize(2);
  display.print(t);                  // Ausgeben der Temperatur
  display.print((char)247); // degree symbol 
  display.println("C");
  display.display();
}
