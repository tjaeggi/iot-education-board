/*
 Title:       read analog Signal from LDR_N, LDR_S, LDR_W, LDR_E with ADS1115 - Analog Digital Converter
 
 Sketchfile:  LDRADC1115SingleEnded.ino
 
 Libraries used: Wire for I2C, Adafruit_ADS1X15 for ADS1115
 
 Function:    Read analog Signal from 4 LDRs and convert it into Voltage, using ADS1115.
              
 
 Connections: LDR_North connected to A0 of ADS1115
              LDR_South connected to A1 of ADS1115
              LDR_West connected to A2 of ADS1115
              LDR_East connected to A3 of ADS1115

 Date:        tja/5.12.2021

*/

#include <Adafruit_ADS1X15.h>

Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */
//Adafruit_ADS1015 ads;     /* Use this for the 12-bit version */

void setup(void)
{
  Serial.begin(115200);
  Serial.println("Hello!");

  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV/ADS1015, 0.1875mV/ADS1115)");

  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV

  if (!ads.begin()) {
    Serial.println("Failed to initialize ADS.");
    while (1);
  }
}

void loop(void)
{
  int16_t adc0, adc1, adc2, adc3;
  float voltsN, voltsS, voltsW, voltsE;

  adc0 = ads.readADC_SingleEnded(0);
  adc1 = ads.readADC_SingleEnded(1);
  adc2 = ads.readADC_SingleEnded(2);
  adc3 = ads.readADC_SingleEnded(3);

  voltsN = ads.computeVolts(adc0);
  voltsS = ads.computeVolts(adc1);
  voltsW = ads.computeVolts(adc2);
  voltsE = ads.computeVolts(adc3);

  Serial.println("----Je höher die Spannung - je heller ist die Umgebung des LDR---------------");
  Serial.print("Nord: "); Serial.print(adc0); Serial.print("  "); Serial.print(voltsN,2); Serial.println("V");
  Serial.print("Süd:  "); Serial.print(adc1); Serial.print("  "); Serial.print(voltsS,2); Serial.println("V");
  Serial.print("West: "); Serial.print(adc2); Serial.print("  "); Serial.print(voltsW,2); Serial.println("V");
  Serial.print("Ost:  "); Serial.print(adc3); Serial.print("  "); Serial.print(voltsE,2); Serial.println("V");

  delay(1000);
}
