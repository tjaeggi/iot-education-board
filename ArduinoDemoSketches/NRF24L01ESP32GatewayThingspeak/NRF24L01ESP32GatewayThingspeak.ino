/*
 * Function:  
 * Receives Sensor-Data with NRF24L01 over 2.4GHz from SensorNode and acts as a Gateway 
 * to the thingspeak.com-Cloud.
 * 
 * Use Sketch together with NRF24L01SensorNodeSender.ino
 * 
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      NRF24L01ESP32GatewayThingspeak.ino
 * Libraries to install:   RF24 TMRh20 2020 - Optimized fork of the nRF24L01+ driver
 * Connections:
 * 
 * NRF24L01     ESP32
 *===================
 *CE            4
 *SCK           18
 *MISO          19
 *CNS=CSN       5
 *MOSI          23
 *IRQ           not used
 *              
 * Power activate: Jumper over Marker NRF24L01 3V3
 * 
 * YOU NEED A WRITE API-KEY !! Note it below into the apiKey-Variable.
 * Chang your SSID-Settings!
 * 
 * Output:    Serial Monitor with 115200baud and thingspeak.com
 * Author:    Thomas Jäggi
 * Date:      05.01.2022
 * 
 */
#include <WiFi.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
 
String apiKey = "CKV42YZ1J617FQC5";  // get it from thingspeak.com. 
                                     //You have to register. It's free!
 
const char* ssid = "YourSSID";
const char* password = "YourPassword";
 
const char* server = "api.thingspeak.com";
 
RF24 radio(4, 5); // CE, CSN on ESP32
const uint64_t address = 0xF0F0F0F0E1LL; // Make sure, Sender has the same address!!
// Adresse ist hier als HEX-Wert dargestellt. Kann beliebig geändert werden.
// Der Empfänger muss dieselbe Adresse haben.
// Bedeutung von LL am Ende: LL is the suffix for long-long, 
// which is 64-bit on most C/C++ implementations. 
// So 0LL is a 64-bit literal with the value of 0. 
struct MyData 
{
  int counter;
  float temperature;
  float humidity;
  float altitude;
  float pressure;
};
MyData data;
 
WiFiClient client;
 
void setup() 
{
  Serial.begin(115200);
  radio.begin();
  
 
  Serial.println("Receiver Started....");
  Serial.print("Connecting to ");
  Serial.println(ssid);
  Serial.println();
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
 
  radio.openReadingPipe(0, address);   //Setting the address at which we will receive the data
  radio.setPALevel(RF24_PA_MIN);       //You can set this as minimum or maximum depending on the distance between the transmitter and receiver.
  radio.startListening();              //This sets the module as receiver
}
 
int recvData()
{
  if ( radio.available() ) 
  {
    radio.read(&data, sizeof(MyData));
    return 1;
    }
    return 0;
}
 
 
void loop()
{
  if(recvData())
  {
 
  Serial.println("Data Received:");
  Serial.print("Packet No. = ");
  Serial.println(data.counter);
  
  Serial.print("Temperature = ");
  Serial.print(data.temperature);
  Serial.println("*C");
 
  Serial.print("Pressure = ");
  Serial.print(data.pressure);
  Serial.println("hPa");
 
  Serial.print("Approx. Altitude = ");
  Serial.print(data.altitude);
  Serial.println("m");
 
  Serial.print("Humidity = ");
  Serial.print(data.humidity);
  Serial.println("%");
 
  Serial.println();
 
  if (client.connect(server, 80)) 
  {
        String postStr = apiKey;
        postStr += "&field1=";
        postStr += String(data.temperature);
        postStr += "&field2=";
        postStr += String(data.pressure);
        postStr += "&field3=";
        postStr += String(data.altitude);
        postStr += "&field4=";
        postStr += String(data.humidity);
        postStr += "\r\n\r\n\r\n\r\n";
        
        client.print("POST /update HTTP/1.1\n");
        client.print("Host: api.thingspeak.com\n");
        client.print("Connection: close\n");
        client.print("X-THINGSPEAKAPIKEY: " + apiKey + "\n");
        client.print("Content-Type: application/x-www-form-urlencoded\n");
        client.print("Content-Length: ");
        client.print(postStr.length());
        client.print("\n\n");
        client.print(postStr);
        delay(1000);
        Serial.println("Data Sent to Server");
      }
        client.stop();
  }
}
