/*
 * Function:  Motor Control with TB6612FNG-Motor-Driver. Turn Clockwise-STOP-Turn CounterClock-BREAK-STANDBY- repeat
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      13-TB6612FNG_MotorTests.ino
 * Connections: AIN1=GPIO16; AIN2=GPIO17; STDBY=GPIO5; PWMA=GPIO4
 * Author:    Thomas Jäggi
 * Date:      30.07.2021
 */
#define PWMA_KANAL_0 0
#define PWMA_AUFLOESUNG 8
#define PWMA_FREQ 5000
#define PWMA_PIN 4  // PWM-Pin
#define AIN1 16
#define AIN2 17
#define STDBY 5  // STDBY muss high sein, damit motor läuft
#define SPEED 255   //0..255; 255 is full-speed

#define CW      (digitalWrite(STDBY,HIGH),digitalWrite(AIN1,HIGH),digitalWrite(AIN2,LOW),ledcWrite(PWMA_KANAL_0,SPEED))
#define CCW     (digitalWrite(STDBY,HIGH),digitalWrite(AIN1,LOW),digitalWrite(AIN2,HIGH),ledcWrite(PWMA_KANAL_0,SPEED))
#define STOP    (digitalWrite(STDBY,HIGH),digitalWrite(AIN1,LOW),digitalWrite(AIN2,LOW), ledcWrite(PWMA_KANAL_0,0))
#define BRAKE   (digitalWrite(STDBY,HIGH),digitalWrite(AIN1,HIGH),digitalWrite(AIN2,LOW), ledcWrite(PWMA_KANAL_0,0))
#define STANDBY (digitalWrite(STDBY,LOW),digitalWrite(AIN1,HIGH),digitalWrite(AIN2,LOW), ledcWrite(PWMA_KANAL_0,0))

// int maxTV = pow(2,PWMA_AUFLOESUNG)-1; // Bei 8 Bit = 0-255


/* TB6612FNG Control Function
 *       INPUT      OUTPUT
 *  INA1  INA2  PWM Mode
 *  ===========================
 *  H     H     H/L Short Brake
 *  L     H     H   CCW
 *  L     H     L   Short Brake
 *  H     L     H   CW
 *  H     L     L   Short Brake
 *  L     L     H   Stop
 *  
 * https://gitlab.com/tjaeggi/iot-education-board/-/blob/master/DatasheetsComponents/TB6612FNG_Datasheet.pdf
 */



void setup() {
pinMode(AIN1,OUTPUT);
pinMode(AIN2,OUTPUT);
pinMode(STDBY,OUTPUT);
pinMode(PWMA_PIN,OUTPUT);
ledcSetup(PWMA_KANAL_0,PWMA_FREQ,PWMA_AUFLOESUNG);
ledcAttachPin(PWMA_PIN, PWMA_KANAL_0);
}



void loop() {
CW;
delay(5000);
STOP;
delay(1000);
CCW;
delay(2000);
BRAKE;
delay(1000);
STANDBY;
delay(5000);
}
