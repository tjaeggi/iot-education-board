/*
 * Function:  Read Pressure and Temperatur from BMP388
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      BMP388withOLED.ino
 * Library:   Adafruit_BMP3XX, Adafruit GFX, Adafruit SSD106, Adafruit Unified Sensor Library wird verwendet.
 * Connections: SCL and SDA from BMP388 to SCL und SDA from ESP32. SCL und SDA from OLED to Microcontroller.
 *              Activate Power with Jumper "OLED 3V3" and "BMP280/388 3V3". Place Jumper over white field.
 * Output:    Serial Monitor with 115200baud and OLED-Display
 * Author:    Thomas Jäggi
 * Date:      07.12.2021
 * Modified from Example-Sketch. See  Complete project details: https://RandomNerdTutorials.com/esp32-bmp388-arduino/
 */
 
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BMP3XX.h"

#define SEALEVELPRESSURE_HPA (1008)  // Wert nachschauen für aktuellen Standort:
// https://meteologix.com/observations/pressure-qnh.html
//*************OLED Settings***************************************************************
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3C for 128x64
//******************************************************************************************
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

Adafruit_BMP3XX bmp;

void setup() {
  Serial.begin(115200);
     if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
       Serial.println(F("SSD1306 allocation failed"));
     for(;;); // Don't proceed, loop forever
     }
  display.clearDisplay();
  //while (!Serial);
  //Serial.println("Adafruit BMP388 / BMP390 test");

  if (!bmp.begin_I2C()) {   // hardware I2C mode, can pass in address & alt Wire
  //if (! bmp.begin_SPI(BMP_CS)) {  // hardware SPI mode  
  //if (! bmp.begin_SPI(BMP_CS, BMP_SCK, BMP_MISO, BMP_MOSI)) {  // software SPI mode
    Serial.println("Could not find a valid BMP3 sensor, check wiring!");
    while (1);
  }

  // Set up oversampling and filter initialization
  bmp.setTemperatureOversampling(BMP3_OVERSAMPLING_8X);
  bmp.setPressureOversampling(BMP3_OVERSAMPLING_4X);
  bmp.setIIRFilterCoeff(BMP3_IIR_FILTER_COEFF_3);
  bmp.setOutputDataRate(BMP3_ODR_50_HZ);
}

void loop() {
  if (! bmp.performReading()) {
    Serial.println("Failed to perform reading :(");
    return;
  }
// Serial Monitor Ausgabe
  Serial.print("Temperature = ");
  Serial.print(bmp.temperature);
  Serial.println(" *C");

  Serial.print("Pressure = ");
  Serial.print(bmp.pressure / 100.0);
  Serial.println(" hPa");

  Serial.print("Approx. Altitude = ");
  Serial.print(bmp.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(" m");

  Serial.println();
  // OLED Ausgabe:
  display.clearDisplay();
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.setTextSize(1);
  display.println("Luftdruck:");
  display.print(bmp.pressure / 100.0);  display.println(" hPa"); // Ausgeben hPa
  display.println("");
  display.println("Hoehe ueber Meer:");
  display.print(bmp.readAltitude(SEALEVELPRESSURE_HPA));display.println(" m");  // Ausgeben HüM
  display.println("");
  display.println("Temperatur:");
  display.print(bmp.temperature);display.print((char)247); display.println("C");
  display.display();
  delay(2000);
}
