/*
 Title:       Read Distance with HC-SR04-Ultrasonic-Sensor
 
 Sketchfile:  HC-SR04-OLED.ino
 
 Libraries used: Wire, Adafruit SSD1306, Adafruit GFX Library
 
 Function:    The HC-SR04 ultrasonic sensor uses sonar to determine the distance to an object. 
              This sensor reads from 2cm to 400cm (0.8inch to 157inch) with an accuracy of 0.3cm (0.1inches), 
              which is good for most hobbyist projects. In addition, this particular module comes with ultrasonic transmitter 
              and receiver modules.
              The ultrasonic sensor uses sonar to determine the distance to an object. Here’s how it works:
              1. The ultrasound transmitter (trig pin) emits a high-frequency sound (40 kHz).
              2. The sound travels through the air. If it finds an object, it bounces back to the module.
              3. The ultrasound receiver (echo pin) receives the reflected sound (echo).
 
 Connections: Activate Power on HC-SR04 5V, 
                                HV 5V, 
                                LV 3V3
                                OLED 3V3
              Jumper Cable Connections:
                                OLED SCL to ESP32 SCL
                                OLED SDA to ESP32 SDA
                                ECHO LV1 to GPIO18
                                Trig to GPIO5
 
 Date:        tja/05.12.2021
*/

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

const int trigPin = 5;
const int echoPin = 18;

//define sound speed in cm/uS
#define SOUND_SPEED 0.034
#define CM_TO_INCH 0.393701

long duration;
int distanceCm;
int distanceInch;

void setup() {
  Serial.begin(115200);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
  delay(500);
  display.clearDisplay();

  display.setTextSize(2);
  display.setTextColor(WHITE);
}

void loop() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  
  // Calculate the distance
  distanceCm = duration * SOUND_SPEED/2;
  
  // Convert to inches
  distanceInch = distanceCm * CM_TO_INCH;
  
  // Prints the distance in the Serial Monitor
  Serial.print("Distance (cm): ");
  Serial.println(distanceCm);
  Serial.print("Distance (inch): ");
  Serial.println(distanceInch);

  display.clearDisplay();
  display.setCursor(0, 0);
  display.setTextSize(1);
  display.println("Move your hand in");
  display.println("front of HC-SR04!");
  display.setCursor(0, 35);
  display.setTextSize(2);
  //Display distance in cm
  display.print(distanceCm);
  display.println(" cm");
  //display.print(distanceInch);
  //display.println(" inch");
  
  // Display distance in inches
  /* display.print(distanceInch);
  display.print(" in");*/
  display.display(); 

  delay(500);  
}
