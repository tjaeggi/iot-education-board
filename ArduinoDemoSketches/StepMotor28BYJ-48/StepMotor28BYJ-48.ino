/*
 Title:       28BYJ-48 Stepper Motor
 
 Sketchfile:  StepMotor28BYJ-48.ino
 
 Libraries used: Stepper built-in by Arduino
 
 Function:    In this sketch, you’ll learn how to control the 28BYJ-48 unipolar stepper motor with the ULN2003 motor driver.
 
 Activate Power on Stepper 5Vext
 
 Connections:      Motor Driver            ESP32
                   ===============================
                   IN1                     GPIO19
                   IN2                     GPIO18
                   IN3                     GPIO5
                   IN4                     GPIO17
 
 siehe auch: https://randomnerdtutorials.com/esp32-stepper-motor-28byj-48-uln2003/
 
 Date:        tja/05.12.2021
*/

#include <Stepper.h>

const int stepsPerRevolution = 2048;  // per step: 0.18 Degrees

// ULN2003 Motor Driver Pins
#define IN1 19
#define IN2 18
#define IN3 5
#define IN4 17

// initialize the stepper library
Stepper myStepper(stepsPerRevolution, IN1, IN3, IN2, IN4);

void setup() {
  // set the speed at 5 rpm
  myStepper.setSpeed(5);
  // initialize the serial port
  Serial.begin(115200);
}

void loop() {
  // step one revolution in one direction:
  Serial.println("counterclockwise");
  myStepper.step(stepsPerRevolution);
  delay(1000);

  // step one revolution in the other direction:
  Serial.println("clockwise");
  myStepper.step(-stepsPerRevolution);
  delay(1000);
}
