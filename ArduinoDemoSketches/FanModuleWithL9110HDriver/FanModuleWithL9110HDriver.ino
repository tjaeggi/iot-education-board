/*
 * Function:  Fan Module with L9110H-Motor-Driver
 * MCU:       Olimex ESP32-DevKit-LiPo
 * Connections: Fan Ctrl INA with GPIO 16, Fan Ctrl INB with GPIO17
 * Power On:  Jumper over white Marker Pins "Fan 5Vext"
 * File:      FanModuleWithL9110HDriver.ino
 * 
 * Author:    Thomas Jäggi
 * Date:      12.03.2023
 */
#define PWM_KANAL_0 0
#define PWM_KANAL_1 1
#define PWM_AUFLOESUNG 8
#define PWM_FREQ 5000
#define INA_PIN 16  // PWM-Pin
#define INB_PIN 17  // PWM-Pin



#define CW      (digitalWrite(INA_PIN,LOW),ledcWrite(INB_KANAL_0,255))
#define CCW     (digitalWrite(INA_PIN,HIGH),ledcWrite(INB_KANAL_0,0))
#define STOP    (digitalWrite(INA_PIN,LOW),digitalWrite(INB_PIN,LOW))

// int maxTV = pow(2,PWMA_AUFLOESUNG)-1; // Bei 8 Bit = 0-255


/*     L9110H Control 
 *    INPUT    
 *  INA  INA    Function
 *  ===========================
 *  L     L     OFF
 *  H     L     FORWARD CW
 *  L     H     REVERSE CCW
 *  H     H     OFF 
 */

int maxspeed = 255;
int minspeed = 0;
int speed=0;

void setup() {
Serial.begin(115200);
pinMode(INA_PIN,OUTPUT);
pinMode(INB_PIN,OUTPUT);
ledcSetup(PWM_KANAL_0,PWM_FREQ,PWM_AUFLOESUNG);
ledcSetup(PWM_KANAL_1,PWM_FREQ,PWM_AUFLOESUNG);
ledcAttachPin(INA_PIN, PWM_KANAL_0);
ledcAttachPin(INB_PIN, PWM_KANAL_1);
}



void loop() {
// Turn Fullspeed Clockwise CW 10sec
Serial.println("Turn Fullspeed Clockwise CW 10sec");
ledcWrite(PWM_KANAL_0,maxspeed);          // INA = 255  
ledcWrite(PWM_KANAL_1,(0));               // INB = 0
delay(10000);
// Motor OFF
Serial.println("Moto off 2sec");
ledcWrite(PWM_KANAL_0,0);                 // INA = 0
ledcWrite(PWM_KANAL_1,0);                 // INB = 0
delay(2000);
// Turn Fullspeed Counter-Clockwise CCW
Serial.println("Turn Fullspeed Counter-Clockwise CCW 10sec");
ledcWrite(PWM_KANAL_0,0);                 // INA = 0
ledcWrite(PWM_KANAL_1,maxspeed);          // INB = 255
delay(10000);
// Motor OFF
Serial.println("Moto off 2sec");
ledcWrite(PWM_KANAL_0,0);                 // INA = 0
ledcWrite(PWM_KANAL_1,0);                 // INB = 0
delay(2000);
// Increase Fan from minspeed to maxspeed - Clockwise
Serial.println("Increase Fan from minspeed to maxspeed - Clockwise");

 for (int i = 100; i <= 255; i++) {
    ledcWrite(PWM_KANAL_0,i);          // INA = 0..255  
    ledcWrite(PWM_KANAL_1,(0));        // INB = 0
    delay(100);
  }

// Decrease Fan from Fullspeed to minspeed - Clockwise
Serial.println("Decrease Fan from Fullspeed to minspeed - Clockwise");
 for (int i = 255; i >= 0; i--) {
    ledcWrite(PWM_KANAL_0,i);          // INA = 255..0  
    ledcWrite(PWM_KANAL_1,0);          // INB = 0
    delay(100);
  }

// Increase Fan from minspeed to maxspeed - Counter-Clockwise
Serial.println("Increase Fan from minspeed to maxspeed - Counter-Clockwise");
 for (int i = 100; i <= 255; i++) {
    ledcWrite(PWM_KANAL_0,0);          // INA = 0  
    ledcWrite(PWM_KANAL_1,i);          // INB = 0..255
    delay(100);
  }

// Decrease Fan from Fullspeed to minspeed - Counter-Clockwise
Serial.println("Decrease Fan from Fullspeed to minspeed - Counter-Clockwise");
 for (int i = 255; i >= 0; i--) {
    ledcWrite(PWM_KANAL_0,0);          // INA = 0  
    ledcWrite(PWM_KANAL_1,i);          // INB = 255..0
    delay(100);
  }



delay(1000);


}
