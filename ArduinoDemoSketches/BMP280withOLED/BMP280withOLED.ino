/*
 * Function:  Read Pressure and Temperatur from BMP280
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      BMP280withOLED.ino
 * Libraries:   BMx280MI, Adafruit GFX und Adafruit SSD106 Library wird verwendet.
 * Connections: SCL and SDA from BMP280 to SCL und SDA from ESP32. SCL und SDA from OLED to Microcontroller.
 *              Activate Power with Jumper "OLED 3V3" and "BMP280/388 3V3". Place Jumper over white field.
 * Output:    Serial Monitor with 115200baud and OLED-Display
 * Author:    Thomas Jäggi
 * Date:      07.12.2021
 * Modified from Example-Sketch 
 */

#include <BMx280I2C.h>
#include <Wire.h>
//*************OLED Settings***************************************************************
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3C for 128x64
//******************************************************************************************
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define I2C_ADDRESS_BMP280 0x76

//create a BMx280I2C object using the I2C interface with I2C Address 0x76
BMx280I2C bmx280(I2C_ADDRESS_BMP280);

void setup() {
  // put your setup code here, to run once:
	Serial.begin(115200);
    if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
     Serial.println(F("SSD1306 allocation failed"));
     for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay();

	Wire.begin();

	//begin() checks the Interface, reads the sensor ID (to differentiate between BMP280 and BME280)
	//and reads compensation parameters.
	if (!bmx280.begin())
	{
		Serial.println("begin() failed. check your BMx280 Interface and I2C Address.");
		while (1);
	}

	if (bmx280.isBME280())
		Serial.println("sensor is a BME280");
	else
		Serial.println("sensor is a BMP280");

	//reset sensor to default parameters.
	bmx280.resetToDefaults();

	//by default sensing is disabled and must be enabled by setting a non-zero
	//oversampling setting.
	//set an oversampling setting for pressure and temperature measurements. 
	bmx280.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
	bmx280.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);

	//if sensor is a BME280, set an oversampling setting for humidity measurements.
	if (bmx280.isBME280())
		bmx280.writeOversamplingHumidity(BMx280MI::OSRS_H_x16);
}

void loop() {
  // put your main code here, to run repeatedly:

	delay(1000);

	//start a measurement
	if (!bmx280.measure())
	{
		Serial.println("could not start measurement, is a measurement already running?");
		return;
	}

	//wait for the measurement to finish
	do
	{
		delay(100);
	} while (!bmx280.hasValue());
// Serial Monitor Ausgabe
	Serial.print("Pressure: "); Serial.print(bmx280.getPressure()/100);Serial.println(" hPa");
	Serial.print("Pressure (64 bit): "); Serial.print(bmx280.getPressure64()/100);Serial.println(" hPa");
	Serial.print("Temperature: "); Serial.print(bmx280.getTemperature());Serial.println(" *C");
  Serial.println(" ");
// OLED Ausgabe:
  display.clearDisplay();
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.setTextSize(2);
  display.println("Luftdruck:");
  display.print(bmx280.getPressure()/100);  display.println(" hPa"); // Ausgeben hPa
  //display.println("Pressure 64Bit:");
  //display.print(bmx280.getPressure64()/100);display.println("hPa");  // Ausgeben hPa 64 Bit
  display.println("Temp:");
  display.print(bmx280.getTemperature());display.print((char)247); display.println("C");
  display.display();
	//important: measurement data is read from the sensor in function hasValue() only. 
	//make sure to call get*() functions only after hasValue() has returned true. 
	if (bmx280.isBME280())
	{
		Serial.print("Humidity: "); 
		Serial.println(bmx280.getHumidity());
	}
}
