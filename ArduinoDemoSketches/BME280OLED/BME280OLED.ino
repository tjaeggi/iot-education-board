/*
 * Function:  Read Pressure and Temperatur from BME280
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      BME280OLED.ino
 * Libraries:   BME280 from Adafruit, Adafruit GFX und Adafruit SSD1306 Library wird verwendet.
 * Connections: SCL and SDA from BME280 to SCL und SDA from ESP32. SCL und SDA from OLED to Microcontroller.
 *              Activate Power with Jumper "OLED 3V3" and "BME280/388 3V3". Place Jumper over white field.
 * Output:    Serial Monitor with 115200baud and OLED-Display
 * Author:    Thomas Jäggi
 * Date:      07.12.2021
 * Modified from Example-Sketch 
 */
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
//*************OLED Settings***************************************************************
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3C for 128x64
//******************************************************************************************
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define SEALEVELPRESSURE_HPA (1035)   // see actual value from 
                                      // https://meteologix.com/observations/pressure-qnh.html

Adafruit_BME280 bme; // I2C

unsigned long delayTime;

void setup() {
    Serial.begin(115200);
        if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
     Serial.println(F("SSD1306 allocation failed"));
     for(;;); // Don't proceed, loop forever
  }
  display.clearDisplay();
  
    while(!Serial);    // time to get serial running
    Serial.println(F("BME280 test"));
    unsigned status;
    status = bme.begin(0x76, &Wire);   //I2C Address BME280: 0x76
  
    Serial.println("-- Default Test --");
    delayTime = 1000;

    Serial.println();
}


void loop() { 
    printValues();
    delay(delayTime);
}


void printValues() {
    Serial.print("Temperature = ");
    Serial.print(bme.readTemperature());
    Serial.println(" °C");

    Serial.print("Pressure = ");

    Serial.print(bme.readPressure() / 100.0F);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(bme.readHumidity());
    Serial.println(" %");

    Serial.println();


//////////// Ausgabe auf OLED ///////////////////////////////////
    display.clearDisplay();
    display.setTextSize(1);      // Normal 1:1 pixel scale
    display.setTextColor(SSD1306_WHITE); // Draw white text
    display.setCursor(0, 0);     // Start at top-left corner
    display.setTextSize(1);
    display.print("Luftdruck: ");display.print(bme.readPressure() / 100.0F);  display.println(" hPa"); // Ausgeben hPa
    //display.println("Pressure 64Bit:");
    //display.print(bmx280.getPressure64()/100);display.println("hPa");  // Ausgeben hPa 64 Bit
    display.print("Temp: ");display.print(bme.readTemperature());display.print((char)247); display.println("C");
    display.print("Feuchtigk.: ");display.print(bme.readHumidity());display.println("%");
    display.print("m ueber Meer: ");display.print(bme.readAltitude(SEALEVELPRESSURE_HPA));display.println("m");
    display.display();
}
