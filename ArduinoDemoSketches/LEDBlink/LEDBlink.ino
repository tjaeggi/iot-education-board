/*
 Title:       LED blink
 Sketchfile:  LEDBlink.ino
 
 Function:    Turns an LED on for one second, then off for one second, repeatedly.
 Connections: Connect Pin "red"  with  ESP32-Pin 23

 Date:        tja/15.10.2021

*/

const int LED_red = 23; 
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_red, OUTPUT);
}
// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_red, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(LED_red, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second
}
