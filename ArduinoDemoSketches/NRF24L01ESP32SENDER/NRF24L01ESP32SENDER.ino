/*
 * Function:  
 * Send Data with NRF24L01 over 2.4GHz This Sketch is the Sender-Part and works 
 * together with NRF24L01ESP32RECEIVER.ino
 * 
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      NRF24L01ESP32SENDER.ino
 * Libraries:   RF24 TMRh20 2020 - Optimized fork of the nRF24L01+ driver
 * Connections:
 * 
 * NRF24L01     ESP32
 *===================
 *CE            4
 *SCK           18
 *MISO          19
 *CNS=CSN       5
 *MOSI          23
 *IRQ           not used
 *              
 * Power activate: Jumper over Marker NRF24L01 3V3
 * 
 * Output:    Browser, Serial Monitor with 115200baud
 * Author:    Thomas Jäggi
 * Date:      05.01.2022
 * 
 */
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define RF24_CHANNEL 83

RF24 radio(4, 5); // CE, CSN on ESP32 
//const uint64_t address = 0xF0F0F0F0E1LL; // Address as a long HEX-Value
const int address = 999;  // change this address to correspond with receiver.
int counter = 0;
 
void setup() 
{
Serial.begin(115200);
radio.begin();                  //Starting the Wireless communication
radio.openWritingPipe(address); //Setting the address where we will send the data
radio.setPALevel(RF24_PA_MIN);  //You can set it as minimum or maximum depending on the distance between the transmitter and receiver.
radio.stopListening();          //This sets the module as transmitter
}
 
void loop()
{
char text[] = "Hi Gebäudeinformatiker!!";
char str[32];
sprintf(str,"%s %d",text,counter);
radio.write(&str, sizeof(str));  
 
Serial.println(str);
counter++;
delay(20);
}
