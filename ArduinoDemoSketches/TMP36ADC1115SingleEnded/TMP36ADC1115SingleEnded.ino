/*
 Title:       read analog Signal from TMP36 with ADS1115 - Analog Digital Converter
 
 Sketchfile:  TMP36ADC1115SingleEnded.ino
 
 Libraries used: Wire for I2C, Adafruit_ADS1X15 for ADS1115
 
 Function:    Read analog Signal from TMP36 and convert it into Voltage, using ADS1115.
              Temperature is derived from Voltage with Formula: temp = (voltage - 0.5) * 100 ; 
 
 Connections: TMP36 Vout is connected to A0 of ADS1115
              SDA ADS1115 to SDA of ESP32
              SCL ADS1115 to SCL of ESP32
 

 Date:        tja/04.12.2021

*/



#include <Wire.h>
#include <Adafruit_ADS1X15.h>

Adafruit_ADS1115 ads1115;

  float volts0=0.000;
  float volts1=0.000;
  float temp=0.000;


void setup(void)
{
  Serial.begin(115200);
  Serial.println("Hello!");
  
  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV)");
  ads1115.begin();
}

void loop(void)
{
  int16_t adc0;

  adc0 = ads1115.readADC_SingleEnded(0);
  //adc1 = ads1115.readADC_SingleEnded(1);
  //adc2 = ads1115.readADC_SingleEnded(2);
  //adc3 = ads1115.readADC_SingleEnded(3);

  //volts1 = ads1115.computeVolts(adc0);
  volts0 = (ads1115.computeVolts(adc0)) * 1.028206821282401; //Korrekturwert für Raumtemperatur. experimentell berechnet.
  //volts2 = ads1115.computeVolts(adc2);
  //volts3 = ads1115.computeVolts(adc3);

  temp = (volts0 - 0.5) * 100 ;  
  
  //Serial.print("AIN0: "); Serial.print(adc0);Serial.print("  ");Serial.println(volts0,3);
  //Serial.print("AIN1: "); Serial.print(adc1);Serial.print(" relates to ");
  Serial.print(volts0,3);Serial.print(" -->");Serial.print(temp,3);Serial.println("*C");
  //Serial.print("AIN2: "); Serial.println(adc2);
  //Serial.print("AIN3: "); Serial.println(adc3);
  Serial.println(" ");
  
  delay(1000);
}
