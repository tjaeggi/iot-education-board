/*
Title:       read analog Signal from Buttons_Vout and derive pressed Button
Sketchfile:  Read5Buttons.ino
 
Function:    Read analog Signal from Buttons_Vout and convert it into Voltage, using ADC and derive pressed Button

================================
Button related Values:
================================
Button      ADC-Value       Volt
================================
no Button   4095            3.3V
UP          340-360         0.3V
DOWN        1080..1090      0.9V
LEFT        1730..1750      1.4V
RIGHT       0               0V
SELECT      2640..2670      2.2V
================================

 Connections: Buttons_Vout is connected to GPIO 34 (Analog ADC1_CH6)

 Date:        tja/21.11.2021

*/


const int BtnPin = 34;

// variable for storing the potentiometer value
int BtnValue = 0;

int btn     = 0;
int adc_key_in  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5


void setup() {
  Serial.begin(115200);
  delay(1000);
  analogSetPinAttenuation(BtnPin, ADC_11db);
  analogReadResolution(12);
  analogSetWidth(12);
  
}

// read the buttons
int read_buttons()
{
 adc_key_in = analogRead(BtnPin);      // read the value from the sensor 
 // float voltage = adc_key_in * (3.3 / 4095.0);
 // Serial.print(adc_key_in); Serial.print(" relates to ");Serial.print(voltage);Serial.println(" Volt");
 // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
 // we add approx 50 to those values and check to see if we are close
 if (adc_key_in > 3500) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
 // For V1.1 us this threshold
 if (adc_key_in < 50)   return btnRIGHT;  
 if (adc_key_in < 500)  return btnUP; 
 if (adc_key_in < 1500)  return btnDOWN; 
 if (adc_key_in < 1800)  return btnLEFT; 
 if (adc_key_in < 3000)  return btnSELECT;  


 return btnNONE;  // when all others fail, return this...
}



void loop() {

 btn = read_buttons();  // read the buttons
  // Reading Btn value
 switch (btn)               // depending on which button was pushed, we perform an action
 {
   case btnRIGHT:
     {
     Serial.println("RIGHT ");
     break;
     }
   case btnLEFT:
     {
     Serial.println("LEFT   ");
     break;
     }
   case btnUP:
     {
     Serial.println("UP    ");
     break;
     }
   case btnDOWN:
     {
     Serial.println("DOWN  ");
     break;
     }
   case btnSELECT:
     {
     Serial.println("SELECT");
     break;
     }
     case btnNONE:
     {
     Serial.println("NO BUTTON PRESSED  ");
     break;
     }
 }

}
