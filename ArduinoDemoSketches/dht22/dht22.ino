/*
 * Function:  Auslesen der Feuchtigkeit und Temperatur mit dem Sensor DHT22
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      dht22.ini
 * Library:   DHT sensor library by Adafruit
 * Connections: DHT22 Dout mit GPIO 16 verbinden
 * Output:    Serial Monitor with 115200baud
 * Author:    Thomas Jäggi
 * Date:      30.07.2021
 */

/************************( Importieren der genutzten Bibliotheken )************************/
#include "DHT.h"                
#define DHTPIN 16         // Hier die Pin Nummer eintragen wo der Sensor angeschlossen ist
#define DHTTYPE DHT22    // Hier wird definiert was für ein Sensor ausgelesen wird. In 
                          // unserem Beispiel möchten wir einen DHT22 auslesen, falls du 
                          // ein DHT22 hast einfach DHT11 eintragen
       
/********************************( Definieren der Objekte )********************************/                          
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(115200);
  Serial.println("DHT22 Testprogramm");
  dht.begin();
}
void loop() {
  // Wait a few seconds between measurements.
  delay(2000);                     // Hier definieren wir die Verweilzeit die gewartet wird  
                                   // bis der Sensor wieder ausgelesen wird. Da der DHT22 
                                   // auch ca. 2 Sekunden hat um seine Werte zu aktualisieren
                                   // macht es keinen Sinn ihn schneller auszulesen!
                                    
  float h = dht.readHumidity();    // Lesen der Luftfeuchtigkeit und speichern in die Variable h
  float t = dht.readTemperature(); // Lesen der Temperatur in °C und speichern in die Variable t
  
/*********************( Überprüfen ob alles richtig Ausgelesen wurde )*********************/ 
  if (isnan(h) || isnan(t)) {       
    Serial.println("Fehler beim auslesen des Sensors!");
    return;
  }

  // Nun senden wir die gemessenen Werte an den PC dise werden wir im Seriellem Monitor sehen
  Serial.print("Luftfeuchtigkeit: ");
  Serial.print(h);                  // Ausgeben der Luftfeuchtigkeit
  Serial.print("%\t");              // Tabulator
  Serial.print("Temperatur: ");
  Serial.print(t);                  // Ausgeben der Temperatur
  Serial.write('*');                // Schreiben des ° Zeichen
  Serial.println("C");
}
