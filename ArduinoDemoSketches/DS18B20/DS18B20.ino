/*
 * Function:  Auslesen der Temperatur mit dem Sensor DS18B20
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      DS18B20.ino
 * Library:   OneWire und DallasTemperature by Miles Burton
 * Connections: "outnaked" oder "outModul" mit GPIO4 verbinden, depending on Sensortype 
 *              Activate Power with Jumper "DS18B20 3V3". Place Jumper over white field.
 * Output:    Serial Monitor
 * Author:    Thomas Jäggi
 * Date:      04.12.2021
 */

#include <OneWire.h>
#include <DallasTemperature.h>

// GPIO where the DS18B20 is connected to
const int oneWireBus = 15;     

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor 
DallasTemperature sensors(&oneWire);

void setup() {
  // Start the Serial Monitor
  Serial.begin(115200);
  // Start the DS18B20 sensor
  sensors.begin();
}

void loop() {
  sensors.requestTemperatures(); 
  float temperatureC = sensors.getTempCByIndex(0);
  //float temperatureF = sensors.getTempFByIndex(0);  // if you also want Fahrenheit
  Serial.print(temperatureC);
  Serial.println("ºC");
  //Serial.print(temperatureF); // if you also want Fahrenheit
  //Serial.println("ºF"); // if you also want Fahrenheit
  delay(1000);
}
