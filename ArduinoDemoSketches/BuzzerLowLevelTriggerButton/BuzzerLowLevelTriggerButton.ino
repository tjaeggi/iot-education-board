/*
 * Sketch: BuzzerLowLevelTrigger_mit_Button.ino
 * This Sketch works with the Buzzer-Module MH-FMD Low Level Trigger
 * Signal = LOW Buzzer is activated
 *
 * 
 */

const int BUTTON_PIN = 16; // GIOP16 pin connected to button's pin
const int BUZZER_PIN = 21; // GIOP21 pin connected to Buzzer's pin "BuzzerIO"

void setup() {
  Serial.begin(115200);                // initialize serial
  pinMode(BUTTON_PIN, INPUT);       // set ESP32 pin to input 
  pinMode(BUZZER_PIN, OUTPUT);       // set ESP32 pin to output mode
}

void loop() {
  int buttonState = digitalRead(BUTTON_PIN); // read new state

  if (buttonState == LOW) {
    Serial.println("The button is being pressed");
    digitalWrite(BUZZER_PIN, LOW);  //Low-Level-Trigger: Buzzer on 
  }
  else
  if (buttonState == HIGH) { 
    Serial.println("The button is unpressed");
    digitalWrite(BUZZER_PIN, HIGH); //Low-Level-Trigger: Buzzer off
  }
}
