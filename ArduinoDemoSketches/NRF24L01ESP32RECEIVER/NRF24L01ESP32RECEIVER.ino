/*
 * Function:  
 * Read Data from NRF24L01. This Sketch is the Receiver-Part and works 
 * together with NRF24L01ESP32SENDER.ino
 * 
 * MCU:       Olimex ESP32-DevKit-LiPo
 * File:      NRF24L01ESP32RECEIVER.ino
 * Libraries:   RF24 TMRh20 2020 - Optimized fork of the nRF24L01+ driver
 * Connections:
 * 
 * NRF24L01     ESP32
 *===================
 *CE            4
 *SCK           18
 *MISO          19
 *CNS=CSN       5
 *MOSI          23
 *IRQ           not used
 *              
 * Power activate: Jumper over Marker NRF24L01 3V3
 * 
 * Output:    Browser, Serial Monitor with 115200baud
 * Author:    Thomas Jäggi
 * Date:      05.01.2022
 * 
 */

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
 
RF24 radio(4, 5); // CE, CSN on ESP32
//const uint64_t address = 0xF0F0F0F0E1LL;  // Address as a long HEX-Value
const int address = 999; // change this address to correspond with sender.
boolean button_state = 0;
 
void setup() 
{
Serial.begin(115200);
radio.begin();
Serial.print("ADDRESS :");
radio.openReadingPipe(0, address);   //Setting the address at which we will receive the data
radio.setPALevel(RF24_PA_MIN);       //You can set this as minimum or maximum depending on the distance between the transmitter and receiver.
radio.startListening();              //This sets the module as receiver
}
void loop()
{
if (radio.available())              //Looking for the data.
{
Serial.print("NRF24L01 snifft auf 2.4GHz ...");Serial.println("");
  
char text[32] = "";                 //Saving the incoming data
radio.read(&text, sizeof(text));    //Reading the data
Serial.print(text);Serial.println("");
}
}
